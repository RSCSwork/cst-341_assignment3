package business;

/*
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import beans.Order;
import beans.Orders;
*/
/**
 * Session Bean implementation class AnotherOrdersBusinessService
 */
//@Stateless
//@LocalBean
//@Alternative


public class AnotherOrdersBusinessService implements OrdersBusinessInterface{
/*
	List<Order> orders = new ArrayList<Order>();
	@Resource(mappedName="java:/ConnectionFactory")
	private ConnectionFactory connectionFactory;

	@Resource(mappedName="java:/jms/queue/Order")
	private Queue queue;
*/		
    /**
     * Default constructor. 
     */
/*
    public AnotherOrdersBusinessService() {
    	//System.out.println("In AnotherOrdersBusinessService default constructor");
    	orders.add(new Order("AnotherOrdersBusiness","Dummy1",1,1));
    	orders.add(new Order("AnotherOrdersBusiness","Dummy2",2,2));
		orders.add(new Order("AnotherOrdersBusiness","Dummy3",3,3));
		orders.add(new Order("AnotherOrdersBusiness","Dummy4",4,4));
		System.out.println("AnotherOrdersBusiness: "+ orders.get(0).getOrderNumber());
    	
    }
*/
	 public void init() {
	    	
	    	System.out.println("init() in the AnotherOrdersBusinessService");
	    	
	    }
	 
	 public void destroy() {
	    	
	    	System.out.println("destroy() from the AnotherOrdersBusinessService");
	    	
	    }

	public void test() {
		System.out.println("Hello from the AnotherOrdersBusinessService");
		//return orders.get(0);
	}
/*
    @Override
	public List<Order> getOrders() {
		
		return orders;
	}

    @Override
	public void setOrders(List<Orders> orders) {
		// TODO Auto-generated method stub
		
	}

    @Override
	public void sendOrder(Order order) {
		try 
		{
			Connection connection = connectionFactory.createConnection();
			Session  session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer messageProducer = session.createProducer(queue);
			TextMessage message1 = session.createTextMessage();
			message1.setText("This is test message");
			messageProducer.send(message1);
			connection.close();
		} 
		catch (JMSException e) 
		{
			e.printStackTrace();
		}		

		
	}
    
*/    
    
    
}
